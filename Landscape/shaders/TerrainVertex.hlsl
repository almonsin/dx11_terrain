#include "HeightMap.hlsli"
#include "TerrainPixelInputType.hlsli"

cbuffer MatrixBuffer
{
    matrix worldMatrix;
    matrix viewMatrix;
    matrix projectionMatrix;
    float4 cameraPos;
};

struct VertexInputType
{
    uint vertexId : SV_VertexID;
};

float4 getGridVertexPositionFromId(uint vertexId)
{
    const uint gridSize = 500;      // should match Terrain.cpp
    const uint vertexCount = gridSize + 1;

    uint quadIndex = vertexId / 6;
    uint vertexIndex = vertexId % 6;

    float x = quadIndex % vertexCount;
    float z = quadIndex / vertexCount;

    static const float4 triangleVertices[6] =    // 2 triangles / quad
    {
        float4(0.0f, 0.0f, 0.0f, 0.0f),
        float4(1.0f, 0.0f, 0.0f, 0.0f),
        float4(0.0f, 0.0f, 1.0f, 0.0f),
        float4(1.0f, 0.0f, 0.0f, 0.0f),
        float4(1.0f, 0.0f, 1.0f, 0.0f),
        float4(0.0f, 0.0f, 1.0f, 0.0f),
    };

    return float4(x - (gridSize / 2), 0, z - (gridSize / 2), 1) + triangleVertices[vertexIndex];
}

PixelInputType main(VertexInputType input)
{
    PixelInputType output;

    float4 position = getGridVertexPositionFromId(input.vertexId);

    // move the grid with the camera
    position.xz += floor(cameraPos.xz);
    
    // set height based on perlin noise
    position.y = getHeight(position.xz);

    // Calculate the position of the vertex against the world, view, and projection matrices.
    output.position = mul(position, worldMatrix);
    output.position = mul(output.position, viewMatrix);
    output.position = mul(output.position, projectionMatrix);
    
    output.uv = position.xz;

    return output;
}
