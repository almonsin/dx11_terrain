#include "PerlinNoise.hlsli"

static const float waterLevel = -10.0f;

float getHeight(float2 pos)
{
    // set height based on perlin noise

    const int COMPONENT_COUNT = 3;

    float2 freqencies[COMPONENT_COUNT] = { float2(0.1f, 0.1f), float2(0.03f, 0.03f), float2(0.01f, 0.01f) };
    float scales[COMPONENT_COUNT] = { 6.0f, 10.0f, 33.0f };

    float height = 0;

    for(int i=0; i<COMPONENT_COUNT; i++)
    {
        height += cnoise(pos * freqencies[i]) * scales[i];
    }

    return max(height, waterLevel);
}
