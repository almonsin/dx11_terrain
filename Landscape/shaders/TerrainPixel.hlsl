#include "HeightMap.hlsli"
#include "TerrainPixelInputType.hlsli"


float4 main(PixelInputType input) : SV_TARGET
{
    // calculate terrain normal
    const float eps = 0.01f;

    float height = getHeight(input.uv);
    float hx = getHeight(input.uv + float2(eps, 0));
    float hz = getHeight(input.uv + float2(0, -eps));

    float3 dx = float3(eps, height-hx, 0);
    float3 dz = float3(0, height-hz, -eps);

    float3 normal = normalize(cross(dx, dz));

    // shade with sun light
    float3 sun = normalize(float3(1,1,0));

    float3 landColor = float3(0.3f, 0.5f, 0.3f);
    float3 waterColor = float3(0.2f, 0.2f, 0.5f);
    float3 color = height <= waterLevel ? waterColor : landColor;

    return float4(color * dot(sun, normal), 1);
}
