#include "pch.h"
#include "Terrain.h"

#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>

#include "ReadData.h"


bool Terrain::init(Microsoft::WRL::ComPtr<ID3D11Device1> device, Microsoft::WRL::ComPtr<ID3D11DeviceContext1> deviceContext, HWND hwnd)
{
	m_device = device;
	m_deviceContext = deviceContext;
	m_hwnd = hwnd;

	return initShaders(L"TerrainVertex.cso", L"TerrainPixel.cso") && initDepthStencil();
}

void Terrain::render()
{
	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	m_deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	if (!setShaderParameters())
	{
		return;
	}

	m_deviceContext->IASetInputLayout(nullptr);

	// Set the vertex and pixel shaders that will be used to render this triangle.
	m_deviceContext->VSSetShader(m_vertexShader.Get(), NULL, 0);
	m_deviceContext->PSSetShader(m_pixelShader.Get(), NULL, 0);

	// Render the terrain
	m_deviceContext->RSSetState(m_renderState.Get());
	m_deviceContext->OMSetDepthStencilState(m_DSState.Get(), 1);

	const unsigned gridSize = 500;
	const unsigned vertexCount = gridSize * gridSize * 3 * 2;   // 2 triangles * 3 vertices per quad
	m_deviceContext->Draw(vertexCount, 0);
}

void Terrain::setCamera(const Vector3 &cameraPos, const Vector3 &cameraForward, float aspectRatio)
{
	m_cameraPos = cameraPos;
	m_proj = Matrix::CreatePerspectiveFieldOfView(DirectX::XMConvertToRadians(60), aspectRatio, 0.1f, 10000.f);
	m_world = Matrix::Identity;
	m_view = Matrix::CreateLookAt(m_cameraPos, m_cameraPos + cameraForward, Vector3::Up);
}

bool Terrain::initShaders(const wchar_t *vsFilename, const wchar_t *psFilename)
{
	std::vector<uint8_t> vertexBlob = DX::ReadData(vsFilename);
	if (FAILED(m_device->CreateVertexShader(&vertexBlob[0], vertexBlob.size(), nullptr, m_vertexShader.GetAddressOf())))
	{
		return false;
	}

	std::vector<uint8_t> pixelBlob = DX::ReadData(psFilename);
	if (FAILED(m_device->CreatePixelShader(&pixelBlob[0], pixelBlob.size(), nullptr, m_pixelShader.GetAddressOf())))
	{
		return false;
	}

	// create constant buffer
	D3D11_BUFFER_DESC constantBufferDesc;
	constantBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	constantBufferDesc.ByteWidth = sizeof(ConstantBuffer);
	constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constantBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	constantBufferDesc.MiscFlags = 0;
	constantBufferDesc.StructureByteStride = 0;

	if (FAILED(m_device->CreateBuffer(&constantBufferDesc, nullptr, m_constantBuffer.GetAddressOf())))
	{
		return false;
	}

	// create render state
	D3D11_RASTERIZER_DESC desc;
	ZeroMemory(&desc, sizeof(D3D11_RASTERIZER_DESC));
	desc.FillMode = D3D11_FILL_SOLID;
	desc.CullMode = D3D11_CULL_BACK;
	if (FAILED(m_device->CreateRasterizerState(&desc, m_renderState.GetAddressOf())))
	{
		return false;
	}

	return true;
}

bool Terrain::setShaderParameters()
{
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	if (FAILED(m_deviceContext->Map(m_constantBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource)))
	{
		return false;
	}

	ConstantBuffer *dataPtr = (ConstantBuffer*)mappedResource.pData;
	dataPtr->world = m_world.Transpose();
	dataPtr->view = m_view.Transpose();
	dataPtr->projection = m_proj.Transpose();
	dataPtr->cameraPos = DirectX::SimpleMath::Vector4(m_cameraPos);

	m_deviceContext->Unmap(m_constantBuffer.Get(), 0);

	unsigned bufferNumber = 0;
	m_deviceContext->VSSetConstantBuffers(bufferNumber, 1, m_constantBuffer.GetAddressOf());

	return true;
}

bool Terrain::initDepthStencil()
{
	D3D11_DEPTH_STENCIL_DESC dsDesc;

	// Depth test parameters
	dsDesc.DepthEnable = true;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS;

	// Stencil test parameters
	dsDesc.StencilEnable = false;

	m_device->CreateDepthStencilState(&dsDesc, m_DSState.GetAddressOf());

	return true;
}
