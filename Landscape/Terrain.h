#pragma once

#include <d3d11.h>
#include <DirectXMath.h>

using DirectX::SimpleMath::Vector3;
using DirectX::SimpleMath::Matrix;

class Terrain
{
	using VertexType = Vector3;

	struct ConstantBuffer
	{
		Matrix world;
		Matrix view;
		Matrix projection;
		DirectX::SimpleMath::Vector4 cameraPos;
	};

public:
	bool init(Microsoft::WRL::ComPtr<ID3D11Device1> device, Microsoft::WRL::ComPtr<ID3D11DeviceContext1> deviceContext, HWND hwnd);
	void render();

	void setCamera(const Vector3 &cameraPos, const Vector3 &cameraForward, float aspectRatio);

private:
	bool initShaders(const wchar_t *vsFilename, const wchar_t *psFilename);
	bool setShaderParameters();
	bool initDepthStencil();

private:
	Microsoft::WRL::ComPtr<ID3D11Device1> m_device;
	Microsoft::WRL::ComPtr<ID3D11DeviceContext1> m_deviceContext;
	HWND m_hwnd;

	Microsoft::WRL::ComPtr<ID3D11VertexShader> m_vertexShader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader> m_pixelShader;
	Microsoft::WRL::ComPtr<ID3D11Buffer> m_constantBuffer;
	Microsoft::WRL::ComPtr<ID3D11RasterizerState> m_renderState;
	Microsoft::WRL::ComPtr<ID3D11DepthStencilState> m_DSState;

	DirectX::SimpleMath::Matrix m_world;
	DirectX::SimpleMath::Matrix m_view;
	DirectX::SimpleMath::Matrix m_proj;
	Vector3 m_cameraPos;
};
