# Terrain demo using DirectX 11

## Description

This is a project to demonstrate terrain rendering using DirectX 11.

The application presents a real-time updated view of a run-time generated mountainous landscape.
The user is able to fly around using the keyboard and/or mouse.
The landscape is infinite, so flight is not limited in any direction.
The application doesn't read any mesh input from file for the shape of the landscape.
Landscape generation and rendering is done on the GPU using shaders.

## Project structure

* [Terrain.cpp](Landscape/Terrain.cpp), [Terrain.h](Landscape/Terrain.h)
  * loading shaders
  * setting up render state
  * updating shader parameters
  * rendering with using the input-assembler stage without buffers
* [TerrainVertex.hlsl](Landscape/shaders/TerrainVertex.hlsl)
  * calculates the grid vertex position from the vertex ID
  * moves the grid with the camera so that the terrain is always centered around the camera
  * sets the height of the vertex based on the terrain heightmap
* [TerrainPixel.hlsl](Landscape/shaders/TerrainPixel.hlsl)
  * calculates the surface normal
  * shades he terrain with a directional sun light
* [HeightMap.hlsli](Landscape/shaders/HeightMap.hlsli)
  * used to calculate terrain height based on multiple Perlin noise components

## Running the demo

The executable and compiled shaders are available in the [output](output/) directory.

## Building from sources

This Visual Studio 2017 solution is based on DirectX Tool Kit tutorials:
https://github.com/Microsoft/DirectXTK/wiki/Getting-Started

Before opening the solution, install Direct3D Game VS project templates:
https://github.com/walbourn/directx-vs-templates/raw/master/VSIX/Direct3DUWPGame.vsix

## Screenshot

![screenshot](output/screenshot.png)
